include theos/makefiles/common.mk

TOOL_NAME = capture fetch import open pbcopy pbpaste play post record say
BINPATH = /usr/local/bin

capture_FILES = capture.m
capture_FRAMEWORKS = MobileCoreServices ImageIO CoreGraphics QuartzCore
capture_PRIVATE_FRAMEWORKS = IOSurface
capture_CODESIGN_FLAGS = -Scapture.xml
capture_INSTALL_PATH = $(BINPATH)

fetch_FILES = fetch.m
fetch_PRIVATE_FRAMEWORKS = iTunesStore StoreServices
fetch_CODESIGN_FLAGS = -Sitsd.xml
fetch_INSTALL_PATH = $(BINPATH)

import_FILES = import.m
import_FRAMEWORKS = AVFoundation
import_PRIVATE_FRAMEWORKS = StoreServices
import_CODESIGN_FLAGS = -Sitsd.xml
import_INSTALL_PATH = $(BINPATH)

open_FILES = open.m
open_PRIVATE_FRAMEWORKS = SpringBoardServices
open_CODESIGN_FLAGS = -Sopen.xml
open_INSTALL_PATH = $(BINPATH)

pbcopy_FILES = pbcopy.m
pbcopy_FRAMEWORKS = UIKit
pbcopy_INSTALL_PATH = $(BINPATH)

pbpaste_FILES = pbpaste.m
pbpaste_FRAMEWORKS = UIKit
pbpaste_INSTALL_PATH = $(BINPATH)

play_FILES = play.m
play_FRAMEWORKS = AVFoundation
play_INSTALL_PATH = $(BINPATH)

post_FILES = post.c
post_FRAMEWORKS = CoreFoundation
post_INSTALL_PATH = $(BINPATH)

record_FILES = record.m
record_FRAMEWORKS = AVFoundation CoreVideo CoreMedia QuartzCore
record_PRIVATE_FRAMEWORKS = IOSurface
record_CODESIGN_FLAGS = -Srecord.xml
record_INSTALL_PATH = $(BINPATH)

say_FILES = say.m
say_PRIVATE_FRAMEWORKS = VoiceServices
say_INSTALL_PATH = $(BINPATH)

include $(THEOS_MAKE_PATH)/tool.mk
