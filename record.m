#import <AVFoundation/AVFoundation.h>
#import <CoreVideo/CVPixelBuffer.h>
#import <dispatch/dispatch.h>
#include <mach/mach_time.h>
#include "capture.h"

#define IGNORE_MSG "Warning: Ignoring option -%c\n"
#define OPT_MONO 1
#define OPT_SRATE 2
#define OPT_FPS 4

static BOOL $_stopped=NO;

static void handle_interrupt(int sig) {
  if($_stopped){exit(1);}
  fputs("Terminating...\n",stderr);
  $_stopped=YES;
  CFRunLoopStop(CFRunLoopGetMain());
}

int main(int argc,char** argv) {
  char options=0;
  int fps=24;
  float srate=44100;
  char* audiofn=NULL,*videofn=NULL;
  int opt;
  while((opt=getopt(argc,argv,"O:mr:o:n:"))!=-1){
    if(opt=='m'){options|=OPT_MONO;}
    else if(!optarg){return 1;}
    else if(opt=='O'){audiofn=optarg;}
    else if(opt=='o'){videofn=optarg;}
    else {
      char ierr=(opt=='r' && (options|=OPT_SRATE))?(sscanf(optarg,"%f",&srate)!=1 || srate<=0):
       (opt=='n' && (options|=OPT_FPS))?(sscanf(optarg,"%d",&fps)!=1 || fps<=0):2;
      if(ierr==2){fprintf(stderr,IGNORE_MSG,opt);}
      else if(ierr){
        fprintf(stderr,"-%c: Invalid argument\n",opt);
        return 1;
      }
    }
  }
  if(!audiofn && !videofn){
    fprintf(stderr,"Usage: %s [-O output.caf [-m(ono)] [-r samplerate]] [-o output.mp4 [-n fps]]\n",argv[0]);
    return 1;
  }
  if(signal(SIGINT,handle_interrupt)==SIG_ERR){
    fputs("Error: Cannot handle SIGINT\n",stderr);
    return 2;
  }
  NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
  AVAudioRecorder* recorder;
  if(audiofn){
    AVAudioSession* session=[AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:NULL];
    [session setActive:YES error:NULL];
    CFURLRef URL=CFURLCreateFromFileSystemRepresentation(NULL,
     (const UInt8*)audiofn,strlen(audiofn),false);
    NSError* error;
    recorder=[[AVAudioRecorder alloc] initWithURL:(NSURL*)URL
     settings:[NSDictionary dictionaryWithObjectsAndKeys:
     [NSNumber numberWithInt:(options&OPT_MONO)?1:2],AVNumberOfChannelsKey,
     [NSNumber numberWithFloat:srate],AVSampleRateKey,nil] error:&error];
    if(!recorder){
      fprintf(stderr,"E[AVAudioRecorder]: %s\n",
       error.localizedDescription.UTF8String);
      return 1;
    }
    CFRelease(URL);
    [recorder record];
  }
  else {
    if(options&OPT_MONO){fprintf(stderr,IGNORE_MSG,'m');}
    if(options&OPT_SRATE){fprintf(stderr,IGNORE_MSG,'r');}
  }
  AVAssetWriter* videoWriter;
  AVAssetWriterInput* videoInput;
  AVAssetWriterInputPixelBufferAdaptor* videoAdaptor;
  IOSurfaceRef surface;
  dispatch_queue_t bgqueue;
  dispatch_source_t timer;
  if(videofn){
    if(access(videofn,F_OK)!=-1){
      fputs("E: File exists\n",stderr);
      return 1;
    }
    CFURLRef URL=CFURLCreateFromFileSystemRepresentation(NULL,
     (const UInt8*)videofn,strlen(videofn),false);
    NSError* error;
    videoWriter=[[AVAssetWriter alloc] initWithURL:(NSURL*)URL
     fileType:AVFileTypeMPEG4 error:&error];
    if(!videoWriter){
      fprintf(stderr,"E[AVAssetWriter]: %s\n",
       error.localizedDescription.UTF8String);
      return 1;
    }
    CFRelease(URL);
    CGRect frame=[CADisplay mainDisplay].bounds;
    int W=frame.size.width,H=frame.size.height;
    NSNumber* format=[NSNumber numberWithInt:kCVPixelFormatType_32BGRA],
     *width=[NSNumber numberWithInt:W],*height=[NSNumber numberWithInt:H];
    videoInput=[[AVAssetWriterInput alloc]
     initWithMediaType:AVMediaTypeVideo outputSettings:[NSDictionary
     dictionaryWithObjectsAndKeys:AVVideoCodecH264,AVVideoCodecKey,
     width,AVVideoWidthKey,height,AVVideoHeightKey,
     [NSDictionary dictionaryWithObjectsAndKeys:
     AVVideoProfileLevelH264Main41,AVVideoProfileLevelKey,
     [NSNumber numberWithInt:fps],AVVideoMaxKeyFrameIntervalKey,nil],
     AVVideoCompressionPropertiesKey,nil]];
    videoInput.expectsMediaDataInRealTime=YES;
    videoWriter.movieTimeScale=videoInput.mediaTimeScale=fps;
    [videoWriter addInput:videoInput];
    videoAdaptor=[[AVAssetWriterInputPixelBufferAdaptor alloc]
     initWithAssetWriterInput:videoInput sourcePixelBufferAttributes:[NSDictionary
     dictionaryWithObjectsAndKeys:format,kCVPixelBufferPixelFormatTypeKey,
     width,kCVPixelBufferWidthKey,height,kCVPixelBufferHeightKey,nil]];
    surface=IOSurfaceCreate([NSDictionary
     dictionaryWithObjectsAndKeys:format,kIOSurfacePixelFormat,
     width,kIOSurfaceWidth,height,kIOSurfaceHeight,
     [NSNumber numberWithInt:4],kIOSurfaceBytesPerElement,
     [NSNumber numberWithInt:4*W],kIOSurfaceBytesPerRow,
     [NSNumber numberWithUnsignedInt:4*W*H],kIOSurfaceAllocSize,
     [NSNumber numberWithBool:YES],kIOSurfaceIsGlobal,nil]);
    void* surfaceptr=IOSurfaceGetBaseAddress(surface);
    [videoWriter startWriting];
    [videoWriter startSessionAtSourceTime:kCMTimeZero];
    bgqueue=dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
    timer=dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER,0,0,dispatch_get_main_queue());
    dispatch_source_set_timer(timer,DISPATCH_TIME_NOW,NSEC_PER_SEC/fps,1000000);
    dispatch_source_set_event_handler(timer,^{
      IOSurfaceLock(surface,0,NULL);
      CARenderServerRenderDisplay(0,CFSTR("LCD"),surface,0,0);
      IOSurfaceUnlock(surface,0,NULL);
      CVPixelBufferRef buffer;
      CVReturn status=CVPixelBufferPoolCreatePixelBuffer(NULL,videoAdaptor.pixelBufferPool,&buffer);
      if(status==kCVReturnSuccess){
        CVPixelBufferLockBaseAddress(buffer,0);
        memcpy(CVPixelBufferGetBaseAddress(buffer),surfaceptr,4*W*H);
        dispatch_async(bgqueue,^{
          while(!videoInput.readyForMoreMediaData){usleep(1000);}
          static int64_t counter=0;
          [videoAdaptor appendPixelBuffer:buffer withPresentationTime:CMTimeMake(counter++,fps)];
          CVPixelBufferUnlockBaseAddress(buffer,0);
          CVPixelBufferRelease(buffer);
        });
      }
      else {fprintf(stderr,"E[CVPixelBuffer]: %d\n",status);}
    });
    dispatch_resume(timer);
  }
  else if(options&OPT_FPS){fprintf(stderr,IGNORE_MSG,'n');}
  fputs("Recording...\n",stderr);
  CFRunLoopRun();
  if(audiofn){
    [recorder stop];
    [recorder release];
  }
  if(videofn){
    dispatch_release(timer);
    CFRelease(surface);
    dispatch_barrier_sync(bgqueue,^{
      [videoInput markAsFinished];
      [videoWriter finishWriting];
    });
    [videoAdaptor release];
    [videoInput release];
    [videoWriter release];
  }
  [pool drain];
  return 0;
}
