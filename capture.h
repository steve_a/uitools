typedef CFTypeRef IOSurfaceRef;
typedef kern_return_t IOReturn;
extern const CFStringRef kIOSurfaceAllocSize;
extern const CFStringRef kIOSurfaceBytesPerElement;
extern const CFStringRef kIOSurfaceBytesPerRow;
extern const CFStringRef kIOSurfaceHeight;
extern const CFStringRef kIOSurfaceIsGlobal;
extern const CFStringRef kIOSurfacePixelFormat;
extern const CFStringRef kIOSurfaceWidth;
extern IOSurfaceRef IOSurfaceCreate(NSDictionary* properties);
extern IOReturn IOSurfaceLock(IOSurfaceRef surface,uint32_t options,uint32_t* seed);
extern IOReturn IOSurfaceUnlock(IOSurfaceRef surface,uint32_t options,uint32_t* seed);
extern void* IOSurfaceGetBaseAddress(IOSurfaceRef surface);
extern void CARenderServerRenderDisplay(kern_return_t,CFStringRef,IOSurfaceRef surface,int x,int y);

@interface CADisplay
+(CADisplay*)mainDisplay;
-(CGRect)bounds;
@end
