int main(int argc,char** argv) {
  NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
  NSFileHandle* input=[NSFileHandle fileHandleWithStandardInput];
  NSData* data=[input readDataToEndOfFile];
  if(isatty([input fileDescriptor])){
    NSUInteger end=[data length];
    if(end){
      const char* ptr=(const char*)[data bytes];
      if(ptr[--end]=='\n'){data=[data subdataWithRange:NSMakeRange(0,end)];}
    }
  }
  [[UIPasteboard generalPasteboard] setData:data
   forPasteboardType:@"public.utf8-plain-text"];
  [pool drain];
}
